import { AuthProvider } from "@pankod/refine-core";

const mockUsers = [
  {
    username: "user",
    roles: ["user"],
    name: "Пользователь",
    avatar: "/images/shrimp_4.png",
  },
  {
    username: "editor",
    roles: ["editor"],
    name: "Редактор",
    avatar: "/images/shrimp_4.png",
  },
];

const authProvider: AuthProvider = {
  login: ({ username, password, remember }) => {
    const user = mockUsers.find((item) => item.username === username);

    if (user) {
      localStorage.setItem("auth", JSON.stringify(user));
      return Promise.resolve();
    }

    return Promise.reject();
  },
  logout: () => {
    localStorage.removeItem("auth");
    return Promise.resolve();
  },
  checkError: (error) => {
    if (error.status === 401) {
      return Promise.reject();
    }
    return Promise.resolve();
  },
  checkAuth: () => {
    return localStorage.getItem("auth") ? Promise.resolve() : Promise.reject();
  },
  getPermissions: () => {
    const auth = localStorage.getItem("auth");
    if (auth) {
      const parsedUser = JSON.parse(auth);
      return Promise.resolve(parsedUser.roles);
    }
    return Promise.reject();
  },
  getUserIdentity: () => {
    const auth = localStorage.getItem("auth");
    if (auth) {
      const parsedUser = JSON.parse(auth);
      return Promise.resolve(parsedUser);
    }
    return Promise.reject();
  },
};

export default authProvider;
