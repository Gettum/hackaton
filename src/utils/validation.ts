export const validateIsRequired = {
    required: true,
    pattern: new RegExp(/^(?!\s*$).+/),
    message: 'Пожалуйста, заполните поле',
};