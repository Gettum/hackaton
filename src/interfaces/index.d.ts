export type IProduct = {
  id: number;
  nameProduct: string;
  idDiagonal: number;
  idCategory: number;
  idProducers: number;
  picture: string;
  type: string;
  year: string;
  series: string;
};

export type IDiagonal = { idDiagonal: number; size: string };

export type IProducer = { idProducer: number; nameProducer: string };

export type ICategory = { idCategories: number; nameCategories: string };

export type IValue = {
  datetime_value: Moment;
  dtBegin: Moment;
  dtEnd: Moment;
  idAuthor: number;
  idEnumProp: number;
  idMutProp: number;
  idProducts: number;
  id: number;
  isActual: boolean;
  number_value: number;
  string_value: string;
};

export type IMutableProp = {
  idMutProp: number;
  idCluster: number;
  nameMutProp: string;
  isEmptyRow?: boolean;
};

export type ICluster = {
  idCluster: number;
  nameCluster: string;
  definition: string;
};

export type IEnumableDictionary = {
  idEnumDict: number;
  idMutProp: number;
  nameEnum: string;
};
