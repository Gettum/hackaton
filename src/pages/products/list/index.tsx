import {
  BaseKey,
  useCreate,
  useList,
  useNavigation,
  useUpdate,
} from "@pankod/refine-core";
import {
  ICategory,
  ICluster,
  IDiagonal,
  IEnumableDictionary,
  IMutableProp,
  IProducer,
  IProduct,
  IValue,
} from "../../../interfaces";
import "./style.css";
import { Input } from "antd";
import { Fade } from "react-reveal";
import {
  Card,
  Col,
  Collapse,
  Row,
  Space,
  Spin,
  Button,
  Select,
  Checkbox,
  useDrawerForm,
  Drawer,
  Edit,
  Form,
  EditButton,
  Create,
  Tooltip,
} from "@pankod/refine-antd";
import {
  DiffOutlined,
  LoadingOutlined,
  PlusOutlined,
  SaveOutlined,
} from "@ant-design/icons";
import moment from "moment";
import { useEffect, useState } from "react";
import { supabaseBasic } from "utils/supabase";
import { HistoryOutlined } from "@ant-design/icons";
import { DeleteButton } from "components/DeleteButton";

const { Meta } = Card;
const { Panel } = Collapse;
const { Search } = Input;
const { Option } = Select;

export const ProductsList: React.FC = () => {
  const { push } = useNavigation();
  const [filtersState, setFiltersState] = useState<IProduct>({} as any);
  const [searchState, setSearchState] = useState<string | undefined>(undefined);
  const [products, setProductsFromSearch] = useState<any>([]);
  const [checkboxes, setCheckboxes] = useState<number[]>([]);

  const {
    drawerProps,
    form,
    formProps,
    show,
    saveButtonProps,
    id,
    formLoading,
    close,
  } = useDrawerForm<IProduct>({
    action: "edit",
    resource: "products",
    redirect: false,
  });

  const {
    drawerProps: createProps,
    form: createForm,
    formProps: createFormProps,
    show: showCreate,
    saveButtonProps: createButtonProps,
    formLoading: loadingCreate,
    close: closeCreate,
  } = useDrawerForm<IProduct>({
    action: "create",
    resource: "products",
    redirect: false,
  });

  const { mutate: update, isLoading: isSavingLoading, isSuccess } = useUpdate();
  const { mutate: create, isSuccess: isSuccessCreate } = useCreate();

  useEffect(() => {
    if (isSuccess && drawerProps.visible) {
      close();
    }
  }, [isSuccess]);

  useEffect(() => {
    if (isSuccessCreate && createProps.visible) {
      closeCreate();
    }
  }, [isSuccessCreate]);

  const filteredFilters: any[] = [
    {
      field: "idDiagonal",
      operator: "eq",
      value: filtersState.idDiagonal,
    },
    {
      field: "idProducers",
      operator: "eq",
      value: filtersState.idProducers,
    },
    {
      field: "idCategory",
      operator: "eq",
      value: filtersState.idCategory,
    },
  ].filter((i) => i.value);

  const { data, isLoading } = useList<IProduct>({
    resource: "products",
    liveMode: "auto",
    config: {
      pagination: { pageSize: 100 },
    },
  });

  const { data: enumerableDictionary } = useList<IEnumableDictionary>({
    resource: "enumerableDictionary",
    config: { pagination: { current: 0, pageSize: 100 } },
  });

  useEffect(() => {
    const viewProducts = products.map((i: IProduct) => i.id);
    setCheckboxes((c) => c.filter((i) => viewProducts.includes(i)));
  }, [products]);

  useEffect(() => {
    if (!isLoading) setProductsFromSearch(data?.data);
  }, [data]);

  const handleSearch = (emptyString?: true) => {
    const searchProducts = async () => {
      const { data } = await supabaseBasic.rpc("findprod", {
        filters: [
          emptyString ? null : searchState || null,
          filtersState?.idCategory ? String(filtersState?.idCategory) : null,
          filtersState?.idProducers ? String(filtersState?.idProducers) : null,
          filtersState?.idDiagonal ? String(filtersState?.idDiagonal) : null,
        ],
      });
      setProductsFromSearch(data || []);
    };
    searchProducts();
  };

  useEffect(() => {
    if (Object.keys(filtersState).length) handleSearch();
  }, [filtersState]);

  const [activeId, setActiveId] = useState<number>();

  const { data: diagonalData } = useList<IDiagonal>({
    resource: "diagonal",
    config: {
      sort: [
        {
          field: "size",
          order: "asc",
        },
      ],
    },
  });
  const { data: categoriesData } = useList<ICategory>({
    resource: "categories",
  });
  const { data: producerData } = useList<IProducer>({
    resource: "producer",
    config: {
      sort: [
        {
          field: "nameProducer",
          order: "asc",
        },
      ],
    },
  });
  const { data: mutableProps } = useList<IMutableProp>({
    resource: "mutableProperties",
    config: {
      pagination: {
        pageSize: 100,
      },
    },
  });
  const { data: clusters } = useList<ICluster>({
    resource: "clusters",
  });
  const { data: values } = useList<IValue>({
    resource: "values",
    config: {
      pagination: {
        pageSize: 100,
      },
    },
  });

  const getMutableValues = (id: BaseKey) =>
    values?.data
      ?.filter((value) => value.idProducts === id && value.isActual)
      .map((value) => {
        const mutableProp = mutableProps?.data?.find(
          (prop) => prop.idMutProp === value.idMutProp
        );

        const cluster = clusters?.data?.find(
          (cluster) => cluster.idCluster === mutableProp?.idCluster
        );

        return {
          ...value,
          ...mutableProp,
          ...cluster,
        };
      });

  const renderRow = (name: string, value?: string | number) => (
    <Row>
      <Col>{name}:</Col>
      <Col>{value && value !== 0 ? value : "–"}</Col>
    </Row>
  );

  const renderCard = (item: IProduct) => {
    const cover = (
      <div className="img-wrapper">
        <img alt="pic" src={item.picture} />
      </div>
    );

    const description = (
      <Col>
        {renderRow(
          "Производитель",
          producerData?.data.find(
            (producer) => producer.idProducer === item.idProducers
          )?.nameProducer
        )}
        {renderRow(
          "Тип подсветки",
          categoriesData?.data.find(
            (category) => category.idCategories === item.idCategory
          )?.nameCategories
        )}
        {renderRow(
          "Диагональ",
          diagonalData?.data.find(
            (diagonal) => diagonal.idDiagonal === item.idDiagonal
          )?.size
        )}
        {renderRow(
          "Наличие",
          getMutableValues(item.id)?.find(
            (item) => item.nameCluster === "Наличие"
          )?.nameMutProp
        )}
        {renderRow(
          "Срок доставки",
          getMutableValues(item.id)?.find(
            (item) => item.nameCluster === "Срок доставки"
          )?.nameMutProp
        )}
      </Col>
    );

    const fullDescription = (
      <Col>
        {renderRow("Модель", item.type)}
        {renderRow("Серия", item.series)}
        {renderRow("Год выпуска", item.year)}
        {renderRow(
          "Тип доставки",
          getMutableValues(item.id)?.find(
            (item) => item.nameCluster === "Тип доставки"
          )?.nameMutProp
        )}
        {renderRow(
          "Продавец",
          getMutableValues(item.id)?.find(
            (item) => item.nameCluster === "Продавец"
          )?.nameMutProp
        )}
      </Col>
    );

    const onCheck = (e: any) => {
      e?.preventDefault();
      setCheckboxes((c) =>
        c.includes(e.target.value)
          ? c.filter((i) => i !== e.target.value)
          : [...c, e.target.value]
      );
    };

    const onClick: React.ComponentProps<typeof EditButton>["onClick"] = (
      event
    ) => {
      event?.stopPropagation();
      show(item.id);
    };

    const card = (
      <Fade bottom key={item.id}>
        <Card hoverable cover={cover} className="product-card" bordered={false}>
          <Row>
            <Meta title={item.nameProduct} description={description} />
            <Space direction="vertical">
              <Tooltip title="Добавить к сравнению" placement="right">
                <Checkbox
                  className="product-checkbox"
                  disabled={
                    !checkboxes.includes(item.id) && checkboxes.length >= 6
                  }
                  value={item.id}
                  onChange={onCheck}
                />
              </Tooltip>
              <Tooltip title="Редактирование" placement="right">
                <EditButton
                  size="small"
                  recordItemId={item.id}
                  hideText
                  onClick={onClick}
                />
              </Tooltip>
              <DeleteButton id={item.id} setActiveId={setActiveId} />
              <Tooltip title="Просмотр истории" placement="right">
                <Button
                  icon={<HistoryOutlined />}
                  size="small"
                  type="ghost"
                  onClick={(e: any) => push(`history?id=${item.id}`)}
                />
              </Tooltip>
            </Space>
          </Row>
        </Card>
      </Fade>
    );

    return (
      <Panel
        header={card}
        key={item.id}
        className={activeId === item.id ? "hovered-collapse-item" : ""}
      >
        <Fade>{fullDescription}</Fade>
      </Panel>
    );
  };

  const onChangeSearch = (e: any) => {
    setSearchState(e.target.value);
    if (!e.target.value) handleSearch();
  };

  const onChangeProducer = (value: any) =>
    setFiltersState((f) => ({ ...f, idProducers: value }));
  const onChangeCategory = (value: any) =>
    setFiltersState((f) => ({ ...f, idCategory: value }));
  const onChangeDiagonal = (value: any) =>
    setFiltersState((f) => ({ ...f, idDiagonal: value }));
  const onClearFilters = () => {
    setFiltersState({} as any);
    setSearchState(undefined);
    handleSearch(true);
  };

  const filtersBlock = (
    <Card className="products-filters-block">
      <Search
        value={searchState}
        onChange={onChangeSearch}
        className="product-filter-search"
        maxLength={50}
        onSearch={() => handleSearch()}
        placeholder="Поиск..."
      />
      <Select
        value={filtersState.idProducers}
        className="product-filter"
        placeholder="Производитель"
        onChange={onChangeProducer}
      >
        {producerData?.data?.map((i) => (
          <Option key={i.idProducer} value={i.idProducer}>
            {i.nameProducer}
          </Option>
        ))}
      </Select>
      <Select
        value={filtersState.idCategory}
        className="product-filter"
        placeholder="Тип подсветки"
        onChange={onChangeCategory}
      >
        {categoriesData?.data?.map((i) => (
          <Option key={i.idCategories} value={i.idCategories}>
            {i.nameCategories}
          </Option>
        ))}
      </Select>
      <Select
        value={filtersState.idDiagonal}
        className="product-filter"
        placeholder="Диагональ"
        onChange={onChangeDiagonal}
      >
        {diagonalData?.data?.map((i) => (
          <Option key={i.idDiagonal} value={i.idDiagonal}>
            {i.size}
          </Option>
        ))}
      </Select>
      <Button
        disabled={!Object.keys(filtersState).length}
        onClick={onClearFilters}
      >
        Сбросить фильтры
      </Button>
    </Card>
  );

  const onClick = () => {
    const newItem = form.getFieldsValue() as IProduct;

    if (id) {
      update({
        resource: "products",
        successNotification: false,
        id,
        values: {
          nameProduct: newItem.nameProduct,
          series: newItem.series,
          type: newItem.type,
          year: newItem.year,
          idProducers: newItem.idProducers,
          idCategory: newItem.idCategory,
          idDiagonal: newItem.idDiagonal,
          picture: newItem.picture,
        },
      });

      const newMutProps = getMutableValues(id)
        ?.map((item) => ({
          idMutProp: item.idMutProp,
          value: form.isFieldTouched(`idMutProp_${item.idMutProp}`),
        }))
        .filter((item) => item.value)
        .map((item) => form.getFieldValue(`idMutProp_${item.idMutProp}`));

      const oldMutProps = getMutableValues(id)
        ?.map((item) => ({
          idMutProp: item.idMutProp,
          value: form.isFieldTouched(`idMutProp_${item.idMutProp}`),
        }))
        .filter((item) => item.value)
        .map((item) => item.idMutProp);

      oldMutProps?.map((item) => {
        const prevValue = values?.data?.find(
          (value) =>
            value.idProducts === id &&
            value.idMutProp === item &&
            value.isActual
        );

        if (prevValue?.id) {
          update({
            resource: "values",
            successNotification: false,
            id: prevValue?.id,
            values: {
              dtEnd: moment().format("YYYY-MM-DD hh:mm:ss"),
              isActual: false,
            },
          });
        }
      });

      newMutProps?.map((item) =>
        create({
          resource: "values",
          successNotification: false,
          values: {
            idMutProp: item,
            dtBegin: moment().format("YYYY-MM-DD hh:mm:ss"),
            dtEnd: "2099-01-01 01:01:01",
            isActual: true,
            idAuthor: 1,
            idProducts: id,
            idEnumProp:
              values?.data?.find((i) => i.idMutProp === item)?.idEnumProp ??
              undefined,
            number_value:
              values?.data?.find((i) => i.idMutProp === item)?.number_value ??
              undefined,
            string_value:
              values?.data?.find((i) => i.idMutProp === item)?.string_value ??
              undefined,
            datetime_value:
              values?.data?.find((i) => i.idMutProp === item)?.datetime_value ??
              undefined,
          },
        })
      );
    }
  };

  const getMutValue = (value: IValue) => {
    const mutableProp = mutableProps?.data?.find(
      (prop) => prop.idMutProp === value.idMutProp
    );

    const enumerable = enumerableDictionary?.data?.find(
      (e) => e.idMutProp === mutableProp?.idMutProp
    );

    if (value.idEnumProp) return enumerable?.nameEnum || "";
    if (value.number_value) return value.number_value;
    if (value.string_value) return value.string_value;
    if (value.datetime_value) return value.datetime_value;
    return "";
  };

  return isLoading ? (
    <div className="loading-wrapper">
      <Spin />
    </div>
  ) : (
    <>
      {filtersBlock}
      <div className="products">
        <Collapse accordion>
          {data?.data.map((item) => renderCard(item))}
        </Collapse>
      </div>
      {id && (
        <Drawer
          {...drawerProps}
          closable={true}
          className={formLoading ? "drawer-loading" : ""}
        >
          {formLoading ? (
            <Spin />
          ) : (
            <Edit
              saveButtonProps={{
                ...saveButtonProps,
                onClick,
                icon: isSavingLoading ? <LoadingOutlined /> : <SaveOutlined />,
              }}
              recordItemId={id}
              title="Редактирование"
            >
              <Form {...formProps} layout="vertical">
                <Form.Item
                  label="Наименование"
                  name="nameProduct"
                  rules={[{ required: true, message: "Введите наименование" }]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  name="series"
                  label="Серия"
                  rules={[{ required: true, message: "Введите серию" }]}
                >
                  <Input placeholder="Введите серию" />
                </Form.Item>
                <Form.Item
                  name="type"
                  label="Модель"
                  rules={[{ required: true, message: "Введите модель" }]}
                >
                  <Input placeholder="Введите модель" />
                </Form.Item>
                <Form.Item
                  name="year"
                  label="Год выпуска"
                  rules={[{ required: true, message: "Введите год" }]}
                >
                  <Input placeholder="Введите год" />
                </Form.Item>
                <Form.Item
                  name="idProducers"
                  label="Производитель"
                  rules={[{ required: true, message: "Введите производителя" }]}
                >
                  <Select placeholder="Введите производителя">
                    {producerData?.data?.map((i) => (
                      <Option key={i.idProducer} value={i.idProducer}>
                        {i.nameProducer}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
                <Form.Item
                  name="idCategory"
                  label="Тип подсветки"
                  rules={[{ required: true, message: "Введите тип подсветки" }]}
                >
                  <Select placeholder="Введите тип подсветки">
                    {categoriesData?.data?.map((i) => (
                      <Option key={i.idCategories} value={i.idCategories}>
                        {i.nameCategories}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
                <Form.Item
                  name="idDiagonal"
                  label="Диагональ"
                  rules={[{ required: true, message: "Введите диагональ" }]}
                >
                  <Select placeholder="Введите диагональ">
                    {diagonalData?.data?.map((i) => (
                      <Option key={i.idDiagonal} value={i.idDiagonal}>
                        {i.size}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
                <Form.Item
                  name="picture"
                  label="Изображение"
                  rules={[
                    { required: true, message: "Прикрепите изображение" },
                  ]}
                >
                  <Input placeholder="Ссылка" />
                </Form.Item>
                {getMutableValues(id)?.map((value, index) => (
                  <Form.Item
                    label={value?.nameCluster}
                    name={`idMutProp_${value?.idMutProp}`}
                    valuePropName="idMutProp"
                    key={`${id}_${index}}`}
                    rules={[
                      {
                        required: true,
                        message: `Выберите ${value?.nameCluster?.toLowerCase()}`,
                      },
                    ]}
                  >
                    <Select
                      placeholder={value?.nameCluster}
                      defaultValue={value?.idMutProp}
                    >
                      {mutableProps?.data
                        ?.filter((prop) => prop.idCluster === value?.idCluster)
                        ?.map((i) => (
                          <Option key={i.idMutProp} value={i.idMutProp}>
                            {`${i.nameMutProp} ${getMutValue(value)}`}
                          </Option>
                        ))}
                    </Select>
                  </Form.Item>
                ))}
              </Form>
            </Edit>
          )}
        </Drawer>
      )}
      <Drawer
        {...createProps}
        closable={false}
        className={loadingCreate ? "drawer-loading" : ""}
      >
        {loadingCreate ? (
          <Spin />
        ) : (
          <Create
            saveButtonProps={{
              ...createButtonProps,
              icon: isSavingLoading ? <LoadingOutlined /> : <SaveOutlined />,
            }}
            title="Создание"
          >
            <Form {...createFormProps} layout="vertical">
              <Form.Item
                label="Наименование"
                name="nameProduct"
                rules={[{ required: true, message: "Введите наименование" }]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="series"
                label="Серия"
                rules={[{ required: true, message: "Введите серию" }]}
              >
                <Input placeholder="Введите серию" />
              </Form.Item>
              <Form.Item
                name="type"
                label="Модель"
                rules={[{ required: true, message: "Введите модель" }]}
              >
                <Input placeholder="Введите модель" />
              </Form.Item>
              <Form.Item
                name="year"
                label="Год выпуска"
                rules={[{ required: true, message: "Введите год" }]}
              >
                <Input placeholder="Введите год" />
              </Form.Item>
              <Form.Item
                name="idProducers"
                label="Производитель"
                rules={[{ required: true, message: "Введите производителя" }]}
              >
                <Select placeholder="Введите производителя">
                  {producerData?.data?.map((i) => (
                    <Option key={i.idProducer} value={i.idProducer}>
                      {i.nameProducer}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item
                name="idCategory"
                label="Тип подсветки"
                rules={[{ required: true, message: "Введите тип подсветки" }]}
              >
                <Select placeholder="Введите тип подсветки">
                  {categoriesData?.data?.map((i) => (
                    <Option key={i.idCategories} value={i.idCategories}>
                      {i.nameCategories}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item
                name="idDiagonal"
                label="Диагональ"
                rules={[{ required: true, message: "Введите диагональ" }]}
              >
                <Select placeholder="Введите диагональ">
                  {diagonalData?.data?.map((i) => (
                    <Option key={i.idDiagonal} value={i.idDiagonal}>
                      {i.size}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item
                name="picture"
                label="Изображение"
                rules={[{ required: true, message: "Прикрепите изображение" }]}
              >
                <Input placeholder="Ссылка" />
              </Form.Item>
            </Form>
          </Create>
        )}
      </Drawer>
      <div
        className={
          checkboxes.length > 1 ? "comparator-btn-show" : "comparator-btn-hide"
        }
      >
        <Button
          type="primary"
          onClick={() => push(`comparator?ids=${checkboxes.join()}`)}
          icon={<DiffOutlined />}
        >
          Сравнить: {checkboxes.length}
        </Button>
      </div>
      <Button
        type="primary"
        onClick={() => showCreate()}
        className="add-btn-show"
        icon={<PlusOutlined />}
      >
        Добавить
      </Button>
    </>
  );
};
