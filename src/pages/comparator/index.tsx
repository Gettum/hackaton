import { Empty } from "@pankod/refine-antd";
import { useList } from "@pankod/refine-core";
import { CompareColumn } from "components/CompareColumn";
import {
    ICategory,
    ICluster,
    IDiagonal,
    IMutableProp,
    IProducer,
    IProduct,
    IValue
} from "interfaces";
import './style.css';

export const ComparatorPage = () => {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const ids = urlParams.get('ids')?.split(',');

    const { data, isLoading } = useList<IProduct>({
        resource: "products",
        liveMode: "auto",
        config: {
            filters: [
                {
                    field: "id",
                    operator: "in",
                    value: ids,
                },
            ],
            pagination: { pageSize: 100 }
        },
    });

    const { data: diagonalData } = useList<IDiagonal>({
        resource: "diagonal",
    });
    const { data: categoriesData } = useList<ICategory>({
        resource: "categories",
    });
    const { data: producerData } = useList<IProducer>({
        resource: "producer",
    });
    const { data: mutableProps } = useList<IMutableProp>({
        resource: "mutableProperties",
        config: {
            pagination: {
                pageSize: 100,
            },
        },
    });
    const { data: clusters } = useList<ICluster>({
        resource: "clusters",
    });
    const { data: values } = useList<IValue>({
        resource: "values",
    });

    const getMutableValues = (id: number) =>
        values?.data
            ?.filter((value) => value.idProducts === id && value.isActual)
            .map((value) => {
                const mutableProp = mutableProps?.data?.find(
                    (prop) => prop.idMutProp === value.idMutProp
                );

                const cluster = clusters?.data?.find(
                    (cluster) => cluster.idCluster === mutableProp?.idCluster
                );

                return {
                    ...value,
                    ...mutableProp,
                    ...cluster,
                };
            });

    const extendedData = data?.data.map((i) => {
        return {
            ...i,
            producer: producerData?.data.find(
                (producer) => producer.idProducer === i.idProducers
            )?.nameProducer,
            categoryName: categoriesData?.data.find(
                (category) => category.idCategories === i.idCategory
            )?.nameCategories,
            diagonal: diagonalData?.data.find(
                (diagonal) => diagonal.idDiagonal === i.idDiagonal
            )?.size,
        };
    });

    const dynamicData = data?.data.map((i) => {
        return clusters?.data?.map((clust, index) => {
            return {
                [`prop${index}`]: getMutableValues(i.id)?.find(
                    (item) => item.nameCluster === clust.nameCluster
                )?.nameMutProp
            }
        });
    });

    const dynamic = clusters?.data?.map((i, index) => {
        return { [`prop${index}`]: i.nameCluster }
    });

    const defaultLabels = {
        nameProduct: 'Наименование',
        categoryName: 'Категория',
        diagonal: 'Диагональ',
        producer: 'Производитель',
        series: 'Серия',
        type: 'Вид',
        year: 'Год выпуска',
    };

    const compareTable = (
        <div className='comparator-table'>
            <CompareColumn item={defaultLabels} dynamic={dynamic} />
            {extendedData?.map((i, index) => <CompareColumn item={i} dynamic={dynamicData?.[index]} />)}
        </div>
    );

    return ids?.length ? compareTable : <Empty description='Отсутствуют данные для сравнения, предварительно требуется выбрать элементы в разделе "Главная"' />;
};