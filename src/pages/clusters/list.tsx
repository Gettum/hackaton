import { useList, useModal } from "@pankod/refine-core";
import { Button, List, Select, Table, TagField } from "@pankod/refine-antd";
import { ICluster, IMutableProp, IEnumableDictionary } from "../../interfaces";
import "./style.css";
import { useState } from "react";
import { AddClusterModal } from "components/AddClusterModal";
import { TagsField } from "components/TagsField";
import { AddPropInput } from "components/AddPropInput";

const { Option } = Select;

export const ClustersList: React.FC = () => {
  const [clusterId, setClusterId] = useState(undefined);
  const { visible, show, close } = useModal();

  const { data: clustersData } = useList<ICluster>({
    liveMode: "auto",
    resource: "clusters",
    config: { pagination: { current: 0, pageSize: 1000 } },
  });

  const { data: mutablePropertiesData } = useList<IMutableProp>({
    liveMode: "auto",
    resource: "mutableProperties",
    config: {
      filters: [
        {
          field: "idCluster",
          operator: "eq",
          value: clusterId,
        },
      ],
      pagination: { current: 0, pageSize: 1000 },
    },
    queryOptions: { enabled: Boolean(clusterId) },
  });

  const { data: enumerableDictionary } = useList<IEnumableDictionary>({
    liveMode: "auto",
    resource: "enumerableDictionary",
    config: { pagination: { current: 0, pageSize: 1000 } },
    queryOptions: { enabled: Boolean(clusterId) },
  });

  const renderOptions = (arr?: ICluster[]) => {
    return arr?.map((i) => (
      <Option key={i.idCluster} value={i.idCluster}>
        {i.nameCluster}
      </Option>
    ));
  };

  const onChange = (value: any) => setClusterId(value);

  const tableData = mutablePropertiesData?.data
    ? [...mutablePropertiesData?.data, { isEmptyRow: true }]
    : undefined;

  const clusterDefinition = clustersData?.data?.find(
    (i) => i.idCluster === clusterId
  )?.definition;

  return (
    <>
      <List>
        <div className="clusters-block">
          <div className="cluster-right-block">
            <Select
              className="cluster-select"
              showSearch={false}
              placeholder="Выберите кластер"
              onChange={onChange}
            >
              {renderOptions(clustersData?.data)}
            </Select>
            {clusterDefinition && (
              <div className="cluster-definition">{clusterDefinition}</div>
            )}
          </div>
          <Button onClick={show}>Добавить кластер</Button>
        </div>
        <div className="clusters-props-block">
          {tableData ? (
            <Table rowKey="id" dataSource={tableData}>
              <Table.Column
                dataIndex="nameMutProp"
                title="Свойство"
                render={(value, record: IMutableProp) =>
                  record?.isEmptyRow && clusterId ? (
                    <AddPropInput idCluster={clusterId} />
                  ) : (
                    value
                  )
                }
              />
              <Table.Column
                dataIndex="idMutProp"
                title="Значения"
                render={(value, record: IMutableProp) =>
                  record?.isEmptyRow ? (
                    <></>
                  ) : (
                    <TagsField
                      value={value}
                      enumerableDictionary={enumerableDictionary?.data}
                    />
                  )
                }
              />
            </Table>
          ) : (
            <></>
          )}
        </div>
      </List>
      {visible && <AddClusterModal visible={visible} close={close} />}
    </>
  );
};
