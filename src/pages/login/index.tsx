import React from "react";
import { useLogin } from "@pankod/refine-core";
import {
  Row,
  Col,
  AntdLayout,
  Card,
  Typography,
  Form,
  Input,
  Button,
} from "@pankod/refine-antd";
import "./style.css";
import ParticlesBackground from "components/particles";
import { validateIsRequired } from "utils/validation";

const { Title } = Typography;

export type ILoginForm = {
  username: string;
  password: string;
  remember: boolean;
};

export const Login: React.FC = () => {
  const [form] = Form.useForm<ILoginForm>();

  const { mutate: login } = useLogin<ILoginForm>();

  const CardTitle = (
    <Title level={3} className="title">
      Shrimps
    </Title>
  );

  return (
    <>
      <ParticlesBackground />
      <AntdLayout className="layout">
        <Row justify="center" align="middle" style={{ height: "100vh" }}>
          <Col xs={22}>
            <div className="container">
              <div className="imageContainer">
                <img src="/images/shrimpWhite.png" />
              </div>
              <Card title={CardTitle} headStyle={{ borderBottom: 0 }}>
                <Form<ILoginForm>
                  layout="vertical"
                  form={form}
                  onFinish={(values) => {
                    login(values);
                  }}
                  requiredMark={false}
                  initialValues={{
                    remember: false,
                  }}
                >
                  <Form.Item
                    name="username"
                    label="Имя пользователя (user / editor)"
                    rules={[validateIsRequired]}
                  >
                    <Input size="large" maxLength={50} placeholder="Имя пользователя" />
                  </Form.Item>
                  <Form.Item
                    name="password"
                    label="Пароль (любой)"
                    rules={[validateIsRequired]}
                    style={{ marginBottom: "12px" }}
                  >
                    <Input
                      type="password"
                      placeholder="●●●●●●●●"
                      maxLength={50}
                      size="large"
                    />
                  </Form.Item>
                  <Button style={{ marginTop: "30px" }} type="primary" size="large" htmlType="submit" block>
                    Войти
                  </Button>
                </Form>
              </Card>
            </div>
          </Col>
        </Row>
      </AntdLayout>
    </>
  );
};
