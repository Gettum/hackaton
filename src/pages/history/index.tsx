import { List, Table } from "@pankod/refine-antd";
import { useOne } from "@pankod/refine-core";
import { IProduct } from "interfaces";
import { useEffect, useState } from "react";
import { supabaseBasic } from "utils/supabase";
import './style.css';

export const HistoryPage = () => {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const productId = urlParams.get('id') || '';
    const [tableData, setTableData] = useState<any[]>([]);

    const product = useOne<IProduct>({
        resource: "products",
        id: productId,
    });

    useEffect(() => {
        const getHistory = async () => {
            const { data } = await supabaseBasic.rpc('reportv4', { product_id: productId, date_from: '2022-07-02 10:00:00', date_to: '2022-08-03 23:00:00' });
            data && setTableData(data);
        }
        getHistory();
    }, []);

    return (
        <List title={`Отчет о изменениях ${product?.data?.data?.nameProduct || ""}`}>
            <Table className='history-table' rowKey="id" dataSource={tableData}>
                <Table.Column
                    dataIndex="idvalue"
                    title="Идентификатор"
                />
                <Table.Column
                    dataIndex="clustername"
                    title="Кластер"
                />
                <Table.Column
                    dataIndex="propertyname"
                    title="Название свойства"
                />
                <Table.Column
                    dataIndex="numbervalue"
                    title="Числовое значение"
                />
                <Table.Column
                    dataIndex="stringvalue"
                    title="Строковое значение"
                />
                <Table.Column
                    dataIndex="datetimevalue"
                    title="Значение даты"
                />
                <Table.Column
                    dataIndex="begin"
                    title="Начало действия параметра"
                />
                <Table.Column
                    dataIndex="end"
                    title="Окончание действия параметра"
                />
                <Table.Column
                    dataIndex="author"
                    title="Автор изменения"
                />
                <Table.Column
                    dataIndex="enumvalue"
                    title="Значение из перечисления"
                />
            </Table>
        </List>
    );
};