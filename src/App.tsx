import { Refine } from "@pankod/refine-core";
import {
  Layout,
  ReadyPage,
  notificationProvider,
  ErrorComponent,
} from "@pankod/refine-antd";
import routerProvider from "@pankod/refine-react-router-v6";
import authProvider from "./auth-provider";
import "@pankod/refine-antd/dist/styles.min.css";
import { ProductsList } from "./pages/products/list";
import { useTranslation } from "react-i18next";
import { dataProvider, liveProvider } from "@pankod/refine-supabase";
import { supabaseClient } from "./utils/supabase";
import "./style.css";
import "./antd.less";
import { Login } from "pages/login";
import { ClustersList } from "pages/clusters";
import { Logo } from "components/Logo";
import { ComparatorPage } from "pages/comparator";
import { HistoryPage } from "pages/history";

const App: React.FC = () => {
  const { t, i18n } = useTranslation();

  const i18nProvider = {
    translate: (key: string, params: object) => t(key, params),
    changeLocale: (lang: string) => i18n.changeLanguage(lang),
    getLocale: () => i18n.language,
  };

  return (
    <Refine
      dataProvider={dataProvider(supabaseClient)}
      liveProvider={liveProvider(supabaseClient)}
      routerProvider={routerProvider}
      authProvider={authProvider}
      Layout={Layout}
      LoginPage={Login}
      ReadyPage={ReadyPage}
      notificationProvider={notificationProvider}
      catchAll={<ErrorComponent />}
      i18nProvider={i18nProvider}
      DashboardPage={ProductsList}
      Title={() => <Logo />}
      resources={[
        {
          name: "clusters",
          list: ClustersList,
          options: { label: "Кластеры" },
        },
        {
          name: "comparator",
          list: ComparatorPage,
        },
        {
          name: "history",
          list: HistoryPage,
        }
      ]}
    />
  );
};

export default App;
