import { Button, Form, Input, Modal, useForm } from "@pankod/refine-antd";
import { useCreate } from "@pankod/refine-core";
import { useState } from "react";
import { validateIsRequired } from "utils/validation";

type Props = {
    visible: boolean;
    close: () => void;
}

export const AddClusterModal = (props: Props) => {
    const { visible, close } = props;
    const [form, setForm] = useState({ nameCluster: undefined, definition: undefined });
    const { mutate } = useCreate();

    const hasFields = form.nameCluster && form.definition;

    const onSave = () => {
        mutate({
            resource: "clusters",
            values: form,
            successNotification: {
                message: 'Кластер успешно добавлен',
                type: "success",
            }
        });
        close();
    };

    const onChangeName = (e: any) => setForm((f: any) => ({ ...f, nameCluster: e.target.value }));

    const onChangeDefinition = (e: any) => setForm((f: any) => ({ ...f, definition: e.target.value }));

    const footer = (
        <>
            <Button disabled={!hasFields} onClick={onSave}>Сохранить</Button>
        </>
    );

    return (
        <Modal visible={visible} footer={footer} onCancel={close}>
            <Form layout="vertical">
                <Form.Item
                    label="Название кластера"
                    name="nameCluster"
                    rules={[validateIsRequired]}
                >
                    <Input maxLength={100} onChange={onChangeName} />
                </Form.Item>
                <Form.Item
                    label="Описание кластера"
                    name="definition"
                    rules={[validateIsRequired]}
                >
                    <Input maxLength={100} onChange={onChangeDefinition} />
                </Form.Item>
            </Form>
        </Modal>
    );
};