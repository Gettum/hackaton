import { TagField } from "@pankod/refine-antd";
import { AddEnumDictInput } from "components/AddEnumDictInput";
import { IEnumableDictionary } from "interfaces";

type Props = {
    value: number;
    enumerableDictionary?: IEnumableDictionary[];
};

export const TagsField = (props: Props) => {
    const { value, enumerableDictionary } = props;
    const tags = enumerableDictionary?.filter((i) => i.idMutProp === value);

    return (
        <>
            {tags?.map((i) => <TagField key={i.idEnumDict} value={i.nameEnum} />)}
            <AddEnumDictInput idMutProp={value} />
        </>
    );
};