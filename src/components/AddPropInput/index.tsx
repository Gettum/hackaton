import { Button, Input } from "@pankod/refine-antd";
import { useCreate } from "@pankod/refine-core";
import { useEffect, useState } from "react";
import './style.css';

type Props = {
    idCluster?: number;
};

export const AddPropInput = (props: Props) => {
    const { idCluster } = props;
    const [name, setName] = useState<string | undefined>(undefined);

    const onChange = (e: any) => setName(e.target.value);

    const { mutate, isSuccess, isLoading } = useCreate();

    useEffect(() => {
        setName(undefined);
    }, [isSuccess]);

    const onSave = () => {
        mutate({
            resource: "mutableProperties",
            values: { nameMutProp: name?.trim(), idCluster },
            successNotification: {
                message: 'Свойство успешно добавлено',
                type: "success",
            }
        });
    };

    const valueIsRequired = !(name && name.trim());

    return (
        <>
            <Input.Group compact>
                <Input className='add-prop-input' maxLength={50} value={name} onChange={onChange} placeholder='Добавить свойство' />
                <Button className='add-prop-button' disabled={isLoading || valueIsRequired} onClick={onSave}>+</Button>
            </Input.Group>
        </>
    );
}