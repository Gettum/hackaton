import Particles from "react-tsparticles";
import { loadFull } from "tsparticles";
import "./style.css";

const ParticlesBackground: React.FC = () => {
  const particlesInit = async (main: any) => {
    await loadFull(main);
  };

  return (
    <Particles
      className="particles"
      style={{ zIndex: -1 }}
      id="tsparticles"
      init={particlesInit}
      options={{
        fpsLimit: 120,
        interactivity: {
          events: {
            onHover: {
              enable: true,
              mode: ["grab", "connect"],
              parallax: {
                enable: true,
                force: 150,
                smooth: 10,
              },
            },
            resize: true,
          },
          modes: {
            connect: {
              distance: 100,
              radius: 20,
            },
            grab: {
              distance: 100,
            },
          },
        },
        particles: {
          reduceDuplicates: true,
          color: {
            value: "#ffffff",
          },
          links: {
            color: "#ffffff",
            distance: 70,
            enable: true,
            opacity: 0.3,
            width: 1,
          },
          collisions: {
            enable: true,
          },
          move: {
            distance: 10,
            enable: true,
            random: true,
            speed: 0.3,
            straight: false,
          },
          number: {
            value: 500,
          },
          opacity: {
            value: 0.5,
          },
          shape: {
            type: "circle",
          },
          size: {
            value: { min: 1, max: 2 },
          },
        },
        detectRetina: true,
      }}
    />
  );
};

export default ParticlesBackground;
