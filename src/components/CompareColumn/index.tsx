import './style.css';

type Props = {
    item: any;
    dynamic?: any;
};

export const CompareColumn = (props: Props) => {
    const { item, dynamic } = props;

    return (
        <div className='compare-column'>
            <div className='compare-img-cell'>{item.picture && <img className="compare-img" alt="pic" src={item.picture} />}</div>
            <div className='compare-name-cell'>{item.nameProduct || '-'}</div>
            <div className='compare-cell'>{item.categoryName || '-'}</div>
            <div className='compare-cell'>{item.diagonal || '-'}</div>
            <div className='compare-cell'>{item.producer || '-'}</div>
            <div className='compare-cell'>{item.series || '-'}</div>
            <div className='compare-cell'>{item.type || '-'}</div>
            <div className='compare-cell'>{item.year || '-'}</div>
            {dynamic && dynamic?.map((i: any, index: any) => <div className='compare-cell-dynamic'>{i?.[`prop${index}`] || '-'}</div>)}
        </div>
    )
}