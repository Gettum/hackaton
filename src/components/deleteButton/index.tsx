import {
  DeleteOutlined,
  LoadingOutlined,
  QuestionCircleOutlined,
} from "@ant-design/icons";
import { Button, Popconfirm, Tooltip } from "@pankod/refine-antd";
import { useDelete } from "@pankod/refine-core";
import { IProduct } from "interfaces";

type Props = { id: number; setActiveId: (id?: number) => void };

export const DeleteButton = ({ id, setActiveId }: Props): JSX.Element => {
  const { mutate, isLoading } = useDelete<IProduct>();

  const onClick: React.ComponentProps<typeof Popconfirm>["onConfirm"] = (
    event
  ) => {
    event?.stopPropagation();
    mutate({
      resource: "products",
      id,
    });
  };

  const onButtonClick: React.ComponentProps<typeof Button>["onClick"] = (
    event
  ) => {
    event?.stopPropagation();
    setActiveId(id);
  };

  const onCancel: React.ComponentProps<typeof Popconfirm>["onCancel"] = (
    event
  ) => {
    event?.stopPropagation();
    setActiveId(undefined);
  };

  return (
    <Popconfirm
      icon={<QuestionCircleOutlined />}
      title="Удалить запись?"
      onConfirm={onClick}
      onCancel={onCancel}
      okText="Удалить"
      cancelText="Отмена"
      placement="topRight"
    >
      <Tooltip title="Удаление" placement="right">
        <Button
          icon={isLoading ? <LoadingOutlined /> : <DeleteOutlined />}
          size="small"
          type="ghost"
          danger
          onClick={onButtonClick}
        />
      </Tooltip>
    </Popconfirm>
  );
};
