import './style.css';

export const Logo = () => {
    return (
        <div className='logo-wrapper'>
            <img className="shrimp-logo" src="/images/shrimpWhite.png" />
        </div>
    );
};