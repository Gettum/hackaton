import { Button, Input } from "@pankod/refine-antd";
import { useCreate } from "@pankod/refine-core";
import { useEffect, useState } from "react";
import './style.css';

type Props = {
    idMutProp?: number;
};

export const AddEnumDictInput = (props: Props) => {
    const { idMutProp } = props;
    const [name, setName] = useState<string | undefined>(undefined);

    const onChange = (e: any) => setName(e.target.value);

    const { mutate, isSuccess, isLoading } = useCreate();

    useEffect(() => {
        setName(undefined);
    }, [isSuccess]);

    const onSave = () => {
        mutate({
            resource: "enumerableDictionary",
            values: { nameEnum: name?.trim(), idMutProp },
            successNotification: {
                message: 'Параметр успешно добавлен',
                type: "success",
            },

        });
    };

    const valueIsRequired = !(name && name.trim());

    return (
        <>
            <Input.Group compact className='add-enum-input-group'>
                <Input className='add-enum-input' maxLength={50} value={name} onChange={onChange} placeholder='Добавить параметр' />
                <Button className='add-enum-button' disabled={isLoading || valueIsRequired} onClick={onSave}>+</Button>
            </Input.Group>
        </>
    );
}