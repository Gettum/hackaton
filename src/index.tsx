import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import "./i18n";
import { Spin } from "@pankod/refine-antd";

const root = document.getElementById("root");

ReactDOM.render(
  <React.Suspense
    fallback={
      <div className="app-loading">
        <Spin />
      </div>
    }
  >
    <App />
  </React.Suspense>,
  root
);
